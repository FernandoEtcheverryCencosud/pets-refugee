import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={Home} />
          <Route path="/login" element={Login} />
          <Route path="/register" element={Register} />
          <Route path="/admin/:id" element={Admin} />
          <Route path="/admin/add" element={AddPet} />
          <Route path="/pets" element={Pets} />
          <Route path="/pets/:id" element={Pet} />
          <Route path="/shelters" element={<Shelters />} />
          <Route path="/shelters/:id" element={<Shelter />} />
          <Route path="/*" element={<Error404 />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
